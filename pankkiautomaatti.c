#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* M��ritell��n, ett� kortti on pankkikortti jos sy�tet��n luku 1234 */
/* Luku -1 lopettaa automaatin k�yt�n */


/* Liittym�n lataus toiminnon esittely */
void Liittymanlataus(void);

/* Tilin valinta toiminnon esittely */
void Tilinvalinta(void);

/* Otto toiminnon esittely */
double Otto(double * tilinsaldo0);

/* Saldo toiminnon esittely */
double Saldo(double * tilinsaldo0);

/* Tapahtumat toiminnon esittely */
void Tapahtumat(void);

/* STOP toiminnon esittely */
void STOP(void);

int main(void)

{   int pankkikortti,               /* Automaattiin sy�tett�v� kortti */
        toiminto,                   /* K�ytt�j�n valitsee toiminnon */
        tilinumero;                 /* K�ytt�j�n sy�tt�m� tilinumero */

    char pin[10],                   /* K�ytt�j�n sy�tt�m� pin-koodi tunnistukseen */
         pintxt[10];                /* Tekstitiedoston pin-koodi tunnistukseen */
    FILE *tili;                     /* Tekstitiedosto pin-koodin saamiseen */
    double tilinsaldo = 10000.00;   /* K�ytt�j�n tilin saldo alussa */


    /* Automaatti k�ynnistyy */
    printf("Tervetuloa!\nSyota kortti, ole hyva\n");

    /*Luetaan pankkikortti */
    scanf("%d", &pankkikortti);

    /* Testataan onko sy�tetty kortti pankkikortti */
    while (pankkikortti != 1234){

        /* Kehotetaan k�ytt�j�� sy�tt�m��n kortti */
        printf("Tervetuloa!\nSyota kortti, ole hyva\n");

        /* Luetaan kortti uudestaan */
        scanf("%d", &pankkikortti);

        }

    /* Kysyt��n k�ytt�j�lt� tilinumero */
    printf("Syota tilinumero\n");

    /* Luetaan tilinumero */
    scanf("%d", &tilinumero);

    /* Tarkistetaan tilinumero */
    while (tilinumero != 12345){

        /* Kehotetaan k�ytt�j�� sy�tt�m��n tilinumero */
        printf("Syota tilinumero uudestaan\n");

        /* Luetaan tilinumero uudestaan */
        scanf("%d", &tilinumero);

        }

        /* Avataan tilitiedosto */
        tili = fopen("C:\\pankkiautomaatti\\12345.txt", "r");
        /* Virhetilanteita varten */
        if (( tili = fopen("C:\\pankkiautomaatti\\12345.txt", "r")) == NULL )
        printf ("Virhe tiedoston avaamisessa\n");
        /* Luetaan oikea pin-koodi */
        fscanf(tili, "%s", pintxt);
        /* Suljetaan tilitiedosto */
        fclose(tili);

    /* Kun kortti on todettu pankkikortiksi, jatketaan tunnistautumiseen */
    printf("Nappaile tunnusluku\nSuojaa tunnuslukusi\n\n\nLopuksi paina OK\n");

    /* Luetaan pin-koodi */
    scanf("%s", pin);

    /* Testataan onko pin-koodi oikea */
    while(strcmp(pin, pintxt)!= 0){

        /* Yritet��n pin-koodia uudelleen */
        printf("Nappaile tunnusluku\nSuojaa tunnuslukusi\nLopuksi paina OK\n");

        /* Luetaan pin-koodi */
        scanf("%s", pin);

    }

    do{

    /* Pin-koodi oikein ja valitaan toiminto */
    printf("Valitse jokin seuraavista toiminnoista:\n(Voit lopettaa STOP-nappaimella)\n\n1. Liittyman lataus Puheaika\n\n2. Tilin valinta\n\n3. Otto\n\n4. Saldo\n\n5. Tapahtumat\n\n6. STOP\n\n");

    /* Luetaan k�ytt�j�n valitsema toiminto */
    scanf("%d", &toiminto);

    } while(toiminto < 1 || toiminto > 6);

    /* Siirryt��n k�ytt�j�n valitsemaan toimintoon */
    switch (toiminto){
    case 1:
         Liittymanlataus();
         break;
    case 2:
         Tilinvalinta();
         break;
    case 3:
         Otto(&tilinsaldo);
         break;
    case 4:
         Saldo(&tilinsaldo);
         break;
    case 5:
         Tapahtumat();
         break;
    case 6:
         STOP();
         break;
    }

    return 0;
}

   /* Liittym�n lataus toiminnon toteutus */
   void Liittymanlataus(void){

   printf("Tassa suoritetaan liittyman lataus.");

}

   /* Tilin valinta toiminnon toteutus */
   void Tilinvalinta(void){

   printf("Tassa suoritetaan tilin valinta.");


   }

   /* Otto toiminnon toteutus */
   double Otto(double * tilinsaldo0){

   int  otto,       /* Tililt� nostettava summa */
        muusumma;   /* K�ytt�j�n itse valitsema nostettava summa */

    do {
   /* Tulostetaan k�ytt�j�lle valikko */
   printf("Otto\n\n");
   printf("1. 20 euroa\n2. 40 euroa\n3. 60 euroa\n4. 90 euroa\n5. 140 euroa\n6. 240 euroa\n7. Muu summa\n8. Alkuun\n");

   /* Luetaan k�ytt�j�n valinta */
   scanf("%d", &otto);

    } while(otto < 1 || otto > 8);

   /* Suoritetaan k�ytt�j�n valinta */
   switch(otto){
   case 1:
        *tilinsaldo0 = *tilinsaldo0 - 20.00;
        printf("Tilinsaldo noston jalkeen on %.2lf euroa\n", *tilinsaldo0);
        printf("Ota rahat, %d kpl 20 euroa.\n", otto);
        break;
   case 2:
        *tilinsaldo0 = *tilinsaldo0 - 40.00;
        printf("Tilinsaldo noston jalkeen on %.2lf euroa\n", *tilinsaldo0);
        printf("Ota rahat, %d kpl 20 euroa.\n", otto);
        break;
   case 3:
        *tilinsaldo0 = *tilinsaldo0 - 60.00;
        printf("Tilinsaldo noston jalkeen on %.2lf euroa\n", *tilinsaldo0);
        printf("Ota rahat, %d kpl 20 euroa.\n", otto);
        break;
   case 4:
        *tilinsaldo0 = *tilinsaldo0 - 90.00;
        printf("Tilinsaldo noston jalkeen on %.2lf euroan\n", *tilinsaldo0);
        printf("Ota rahat, 1 kpl 50 euroa ja 2 kpl 20 euroa.\n");
        break;
   case 5:
        *tilinsaldo0 = *tilinsaldo0 - 140.00;
        printf("Tilinsaldo noston jalkeen on %.2lf euroa\n", *tilinsaldo0);
        printf("Ota rahat, 2 kpl 50 euroa ja 2 kpl 20 euroa\n");
        break;
   case 6:
        *tilinsaldo0 = *tilinsaldo0 - 240.00;
        printf("Tilinsaldo noston jalkeen on %.2lf euroa\n", *tilinsaldo0);
        printf("Ota rahat, 4 kpl 50 euroa ja 2 kpl 20 euroa\n");
        break;
   case 7:
        do{
        printf("Syota summa\n");
        scanf("%d", &muusumma);
        } while(muusumma < 50 || muusumma > 1000);
        *tilinsaldo0 = *tilinsaldo0 - muusumma;
        printf("Tilinsaldo noston jalkeen on %.2lf euroa\n", *tilinsaldo0);

        if (muusumma == 50 || muusumma == 100 || muusumma == 200 || muusumma == 300 || muusumma == 400 || muusumma == 500 || muusumma == 600 || muusumma == 700 || muusumma == 800 || muusumma == 900 || muusumma == 1000){
        muusumma = muusumma / 50;
        printf("Ota rahat, %d kpl 50 euroa.\n", muusumma);
        }

        if (muusumma == 110 || muusumma == 210 || muusumma == 310 || muusumma == 410 || muusumma == 510 || muusumma == 610 || muusumma == 710 || muusumma == 810 || muusumma == 910){
        muusumma = (muusumma - 60) / 50;
        printf("Ota rahat, %d kpl 50 euroa ja 3 kpl 20 euroa\n", muusumma);
        }

        if (muusumma == 120 || muusumma == 220 || muusumma == 320 || muusumma == 420 || muusumma == 520 || muusumma == 620 || muusumma == 720 || muusumma == 820 || muusumma == 920){
        muusumma = (muusumma - 20) / 50;
        printf("Ota rahat, %d kpl 50 euroa ja 1 kpl 20 euroa\n", muusumma);
        }

        if (muusumma == 130 || muusumma == 230 || muusumma == 330 || muusumma == 430 || muusumma == 530 || muusumma == 630 || muusumma == 730 || muusumma == 830 || muusumma == 930){
        muusumma = (muusumma - 80) / 50;
        printf("Ota rahat, %d kpl 50 euroa ja 4 kpl 20 euroa\n", muusumma);
        }

        if (muusumma == 340 || muusumma == 440 || muusumma == 540 || muusumma == 640 || muusumma == 740 || muusumma == 840 || muusumma == 940){
        muusumma = (muusumma - 40) / 50;
        printf("Ota rahat, %d kpl 50 euroa ja 2 kpl 20 euroa\n", muusumma);
        }

        if (muusumma == 150 || muusumma == 250 || muusumma == 350 || muusumma == 450 || muusumma == 550 || muusumma == 650 || muusumma == 750 || muusumma == 850 || muusumma == 950){
        muusumma = muusumma / 50;
        printf("Ota rahat, %d kpl 50 euroa.\n", muusumma);
        }

        if (muusumma == 160 || muusumma == 260 || muusumma == 360 || muusumma == 460 || muusumma == 560 || muusumma == 660 || muusumma == 760 || muusumma == 860 || muusumma == 960){
        muusumma = (muusumma - 60) / 50;
        printf("Ota rahat, %d kpl 50 euroa ja 3 kpl 20 euroa\n", muusumma);
        }

        if (muusumma == 170 || muusumma == 270 || muusumma == 370 || muusumma == 470 || muusumma == 570 || muusumma == 670 || muusumma == 770 || muusumma == 870 || muusumma == 970){
        muusumma = (muusumma - 20) / 50;
        printf("Ota rahat, %d kpl 50 euroa ja 1 kpl 20 euroa\n", muusumma);
        }

        if (muusumma == 180 || muusumma == 280 || muusumma == 380 || muusumma == 480 || muusumma == 580 || muusumma == 680 || muusumma == 780 || muusumma == 880 || muusumma == 980){
        muusumma = (muusumma - 80) / 50;
        printf("Ota rahat, %d kpl 50 euroa ja 4 kpl 20 euroa\n", muusumma);
        }

        if (muusumma == 190 || muusumma == 290 || muusumma == 390 || muusumma == 490 || muusumma == 590 || muusumma == 690 || muusumma == 790 || muusumma == 890 || muusumma == 990){
        muusumma = (muusumma - 40) / 50;
        printf("Ota rahat, %d kpl 50 euroa ja 2 kpl 20 euroa\n", muusumma);
        }
        break;
   case 8:
        STOP();
        break;
    }

   return 0;

   }

   /* Saldo toiminnon toteutus */
   double Saldo(double * tilinsaldo0){

   /* Tulostetaan tilin saldo */
   printf("Tilin saldo on %.2lf euroa", *tilinsaldo0);

    return 0;

   }

   /* Tapahtumat toiminnon toteutus */
   void Tapahtumat(void){

   int luku;    /* K�ytt�j�n valinta tapahtumien n�yt�st� */

   do {
   /* Kysyt��n k�ytt�j�lt� valinta */
   printf("Haluatko tiedot 1. Naytolle vai 2. Kuitille?\n");

   /* Luetaan valinta */
   scanf("%d", &luku);
   } while (luku < 1 || luku > 2);

   /* Suoritetaan valinta */
   if (luku == 1)

    printf("Tapahtumat naytetaan tassa.");

   else

    printf("Tapahtumat tulostetaan kuitille.");

   }

   /* STOP toiminnon toteutus */
   void STOP(void){

    int lopetus;    /* Lopetusmerkki */

    do {
    /* Tulostetaan ohje k�ytt�j�lle */
    printf("Jos haluat lopettaa automaatin kayton syota -1\n");

    /* Luetaan lopetusmerkkki */
    scanf("%d", &lopetus);

    } while (lopetus != -1);

    /* Suoritetaan automaatin k�yt�n lopettaminen */
    switch(lopetus){

    case -1:
        printf("Kiitos kaynnista, tervetuloa uudelleen!\n");
        break;
    }
}
